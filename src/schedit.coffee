# Options which can be passed in directly to Schedit(), or as the dataSet of the
# main element
options = null
# The element where the UI is to be placed. This is probably going to be <body>,
# in which case you don't need to set it.
element = null
# Keep track of whether we've started or not.
started = false

start = ->
  console.log("%o.start(%o)", @, arguments)

# The starting function - sets some options, then uses the $ function in the
# current context to call start.
@Schedit = (options) ->
  app ?= new Schedit.App(options)
